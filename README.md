Shellfu
=======

Shellfu attempts to add "library" element to shell languages (mostly
Bash), with features such as path lookups and quick pydoc-like library
reference access.

Why?  See ["why" explanation][1] document, but in short, it's
for cases when, for some reason you cannot use full-fledged platform
such as Perl or Python, but still need to write and even re-distribute
fair amount of code.  Keeping all in single script may lead to huge mess
(repetition, stale code, no style...).

 [1]: https://gitlab.com/vornet/shellfu/shellfu/-/tree/master/notes/intro.md


Project Status and Versioning
-----------------------------

 *  Development stage is something between "experimental" and "alpha",
    that is, lot of tests are yet to be written and only real user is
    its own author.

 *  API (commands, options...) may be changed/removed as needed.

 *  Although that will not happen within same version.

 *  Branch 'latest' contains only the latest released version.  Changes in
    this branch can be API-breaking.

 *  Any other branches are generally treated as development branches and
    are likely to contain bugs.


Installation
------------

**Disclaimer**: Use at your own risk. See Project Status section for
details.


### From packages ###

Fedora packages are available in [COPR repository][2].  Debian packages
are available on-demand (see Contacts section).

  [2]: https://copr.fedorainfracloud.org/coprs/netvor/shellfu/


### From source ###

There are almost no specific dependencies, you can do away with common
GNU/Linux distribution with GNU Make installed.

To get the last source code and install:

    git clone <insert-url-here>
    cd shellfu
    make
    sudo make install

You can also run our humble test suite (~15 seconds):

    make test

NOTE that  the `make test` actually tests the version of Shellfu
*installed* on your machine.  This is a known issue, sorry for
that.


Usage
-----

### Your first script ###

After installing, one of the first things you can do is:

    sfpath
    sfpath --version

That should return path to *shellfu.sh* and version, respectively.
And that's also *all* this command does.

Next, if you have also installed some modules, you can look around
using `sfdoc`:

    sfdoc --lsmod
    sfdoc isa

This is how you access documentation of modules available on the system.
You can also access documentation of a modules directly by specifying
path to the file (the path must contain slash):

    sfdoc path/to/my/module.sh

Note that sfdoc only works if the module follows Shellfu Style
Guide, in particular the Docstrings section.

Next, you can create your first Shellfu-Bash script:

    #!/usr/bin/bash

    . "$(sfpath)" || exit 3

    shellfu import pretty

    think "this will be displayed if PRETTY_VERBOSE is 'true'"
    debug "this will be displayed if PRETTY_DEBUG is 'true'"

    fn() {
        local some_var=some value
        local some_array=(some items)
        debug -v some_var some_array something_else
    }

    warn "I'm getting tired"

    fn

    die "ok I'm done"

This is how above script works:

 1. sfpath provides path to the main shellfu.sh, which defines
    `shellfu()` function (plus a bunch of boring variables).

 2. Using `shellfu import pretty`, we're importing module called "pretty".
    shellfu will figure out where exactly it is.

 3. If we called `sfdoc pretty` in another terminal, we would find
    out that among others, this module contains functions `think`, `debug`,
    `warn` and `die`.

 4. Rest of it is just showing off how cool `pretty` is.


Calling the script is the same as any other bash scripts, what you can
do though, is play with some pretty variables:

    PRETTY_DEBUG=true ./my_script
    PRETTY_VERBOSE=true ./my_script
    PRETTY=color PRETTY_DEBUG=true ./my_script
    PRETTY=notify PRETTY_VERBOSE=true ./my_script


### Your first module ###

In its most basic form, a Shellfu module is nothing special.  All you need
to do is

 1. write a file with .sh extension with some bash functions/variables,

 2. save it somewhere,

 3. in your script, call `shellfu import my_module`--you can now access
    the functions/variables you have defined.

 4. When running your script, set `SHELLFU_PATH` to the location where
    my_module.sh is.

To get the basic stuff, nothing more is really needed.  If you want, however,
your module be explorable by `sfdoc`, you need to follow Shellfu Style
Guide.


And friends
-----------

Apart from Shellfu, there also other "sibling" projects helping you even
furhter:

 *  TFkit -- to help create and run unit test suites for shell functions
    and commands.

 *  MKit -- to help get you started with boring stuff like deployment,
    packaging (deb and rpm) and release management (versioning,
    changelogs).

 *  saturnin -- to help with creating more rich toolkits in form of
    meta-commands.

Not all of them are public, yet (but all of them *will be*).  Well, the first
two are actually integrated *in*, and used to maintain Shellfu codebase.


Contact
-------

Author of Shellfu idea, design and code is Alois Mahdal <netvor+shellfu at vornet dot cz>.

For general questions, visit `#shellfu` at libera.chat.

More communication channels will be added at some point.
