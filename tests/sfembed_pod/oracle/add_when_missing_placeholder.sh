#!/bin/bash
#
# Some arbitrary text like License and authorship info..
#

#
# The test module description.
#

#

#
# A test variable with a short docstring
#
SIMPLE_LIMIT=42

simple_foo() {
    #
    # A test function with a short docstring
    #
    local foo=$1
    echo "$foo"
    echo $SIMPLE_LIMIT
}


This part goes on after the embed section and should
not be affected.
