usage:
  sfembed_pod [options] [-u] FILE.SH
  sfembed_pod [options] -r FILE.SH

commands:
  -u     (default) add or update POD under name NAME from FILE
  -r     remove embedded POD from FILE

options:
  -B BEGIN_MARK   use BEGIN_MARK to demark beginning of the
         embed section in FILE
  -E END_MARK  use END_MARK to demark ending of the embed
         section in FILE.
  -P PLACEHOLDER  use PLACEHOLDER to demark empty embed section
         embedded section
  -n NAME   refer to module as NAME in in POD meta-data and
         outline
  -d     show debugging information
  -i     in-place update; ie. replace original FILE

FILE must be formatted according to Docstrings section of Shellfu
Coding Style[1].

Additionally, it must contain embed section, which will be
replaced with documentation extracted from the FILE.  A valid
embed section is either a single line marking empty section:

    #----- SFDOC EMBEDDED POD PLACEHOLDER -----#

or (when is POD already embedded) following lines in this order:

    #----- SFDOC EMBEDDED POD BEGIN -----#
    [...any number of lines ...]
    [...containing POD already embedded ...]
    #----- SFDOC EMBEDDED POD END -----#

Marker strings can be changed using envvars SFD_EMBED_PLACE,
SFD_EMBED_BEGIN, SFD_EMBED_END or options -P, -B and -E, but they
must always be single line.

  [1]: https://gitlab.com/vornet/shellfu/shellfu/-/blob/master/notes/style.md

bad usage: no FILE?
