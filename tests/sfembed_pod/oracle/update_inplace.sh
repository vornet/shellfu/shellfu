#!/bin/bash
#
# Some arbitrary text like License and authorship info..
#

#
# The test module description.
#

#

#
# A test variable with a short docstring
#
SIMPLE_LIMIT=42

simple_foo() {
    #
    # A test function with a short docstring
    #
    local foo=$1
    echo "$foo"
    echo $SIMPLE_LIMIT
}

#----- SFDOC EMBEDDED POD BEGIN -----#
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !! DO NOT EDIT section between this comment      !!
# !! and the end mark or YOUR CHANGES WILL BE LOST !!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# The end mark should look like this:
#
#     #----- SFDOC EMBEDDED POD END -----#
#
# To update module documentation properly:
#
#  1. Edit in-code docstrings according to Docstrings section
#     of the Shellfu coding style guide:
#
#     <https://gitlab.com/vornet/shellfu/shellfu/-/blob/master/notes/style.md>
#
#  2. Run following command to re-build this section:
#
#         sfembed_pod -n "library/name" -i path/to/lib.sh
#
# Call sfembed_pod --help for more details.
#
# For Fedora-based distributions, sfembed_pod can be found in
# shellfu-devel package in this COPR:
#
# <https://copr.fedorainfracloud.org/coprs/netvor/shellfu/>
#

#shellcheck disable=SC2217
true <<'=cut'
=pod

=encoding utf8

=head1 NAME

update_inplace - The test module description.

=head1 DESCRIPTION

The test module description.


=head1 VARIABLES

=over 8


=item I<$SIMPLE_LIMIT>

A test variable with a short docstring

=back


=head1 FUNCTIONS

=over 8


=item I<simple_foo()>

A test function with a short docstring

=back

=cut
#----- SFDOC EMBEDDED POD END -----#

This part goes on after the embed section and should
not be affected.
