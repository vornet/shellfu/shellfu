#!/bin/bash
#
# Some arbitrary text like License and authourship info
# this time the text is somewhat longer and has its own
# paragraphs.
#
# This is an example of such paragraph, which does span
# more than one, more than two, in fact, actually about
# three lines.
#

#
# A test module with functions, variables and long docstring
#
# This one has even the module docstring longer as it can then
# form a testcase of such module.
#
# # Usage #
#
# This module is only used for testing of docstring parsing
# and object (by which we mean *function* or *variable*, not
# some OOP stuff) printing functionality.
#
# The internal format is Markdown-ish, but is parsed as plain
# text.  But still, as an example we will try to use some
# common Markdown.  For example, this:
#
#     #!/bin/bash
#     #
#
# is like first two lines of this file look like.
#

#
# A test variable with a short docstring
#
PATH_LIMIT=42

#
# A test variable with a long docstring
#
# Not really long but the longer version (header plus
# the rest)
#
PATH_DEBUG=${PATH_DEBUG:-false}

path_foo() {
    #
    # A test function with a short docstring
    #
    local foo=$1
    local bar
    bar=$foo
    echo $foo
}

path_other_function() {
    #
    # A test function with a long docstring
    #
    # Not really long but the longer version (header plus
    # the rest)
    #
    :
}
