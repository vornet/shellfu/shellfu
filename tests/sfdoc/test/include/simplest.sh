
#
# Simplest module possible
#

#
# A test variable with a short docstring
#
SIMPLEST_FOO=${SIMPLEST_FOO:-bar}

simplest__baz() {
    #
    # A test function with a short docstring
    #
    :
}
