#!/bin/bash
#
# Some arbitrary text like License and authourship info
# this time the text is somewhat longer and has its own
# paragraphs.
#
# This is an example of such paragraph, which does span
# more than one, more than two, in fact, actually about
# three lines.
#

# '
here can be code
# or things that look a bit like module docstrings
#


#
# A test module with functions, variables and long docstring
#
# This one has even the module docstring longer as it can then
# form a testcase of such module.
#
# # Usage #
#
# This module is only used for testing of docstring parsing
# and object (by which we mean *function* or *variable*, not
# some OOP stuff) printing functionality.
#
# The internal format is Markdown-ish, but is parsed as plain
# text.  But still, as an example we will try to use some
# common Markdown.  For example, this:
#
#     #!/bin/bash
#     #
#
# is like first two lines of this file look like.
#

#
# This is already NOT part of above doc
#

#
# A test variable with a short docstring
#
TRICKY_LIMIT=42

TRICKY_REVERSED=42
#
# It's also possible to have docstring *after* the doc
#

echo $TRICKY_LIMIT:$TRICKY_REVERSED

#
# A test variable with a long docstring
#
# Not really long but the longer version (header plus
# the rest)
#
TRICKY_DEBUG=${TRICKY_DEBUG:-false}

tricky_foo() {
    #
    # A test function with a short docstring
    #
    #shellfu disable=ZZ
    local foo=$1
    local bar
    bar=$foo
    echo "$bar"
}

tricky_other_function() {
    #
    # A test function with a long docstring
    #
    # Not really long but the longer version (header plus
    # the rest)
    #
    :
}
