%if ( 0%{?rhel} && 0%{?rhel} < 7 ) || ( 0%{?centos} && 0%{?centos} < 7 )
%global pspkg procps
%global manpkg man
%global pod2manpkg perl
%else
%global pspkg procps-ng
%global manpkg man-db
%global pod2manpkg perl-podlators
%endif

%if ( 0%{?rhel} && 0%{?rhel} < 8 ) || ( 0%{?centos} && 0%{?centos} < 8 )
%global legacy_coerce true
%else
%global legacy_coerce false
%endif

Name:           __MKIT_PROJ_PKGNAME__
Version:        __MKIT_PROJ_VERSION__
Release:        1%{?dist}
Summary:        __MKIT_PROJ_NAME__ - __MKIT_PROJ_TAGLINE__
License:        LGPLv2
URL:            __MKIT_PROJ_VCS_BROWSER__
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  %{pspkg}
BuildRequires:  make
BuildRequires:  perl
BuildRequires:  util-linux

Requires: %{pspkg}
Requires: util-linux
%description
Shellfu is an attempt to add modularity to your shell scripts.

With Shellfu you can develop your shell modules separately from your
scripts, and test, use, explore or study them without need to be aware
of details such as where the actual files are placed.

Shellfu is mostly intended for cases when there's need for non-trivial
amount of reliable body of shell scripts, and access to advanced modular
languages such as Python or Perl is limited.

This sub-package contains only the Shellfu management system but not
actual modules.  Use this if you want to run your own modules.

%package bash
Summary: Bash-specific base
Requires: shellfu
%description bash
This sub-package contains Bash-specific parts of library infrastructure.
Shellfu modules written for Bash should depend on this.

%package bash-arr
Summary: Array utilities
Requires: shellfu-bash
Requires: shellfu-bash-pretty
Requires: shellfu-sh-isa
%description bash-arr
This sub-package contains 'arr', Shellfu/Bash module with few utilities
for manipulating Bash arrays

%package bash-pretty
Summary: Logging Shellfu/Bash module
Requires: shellfu-bash
Requires: shellfu-sh-exit
Requires: shellfu-sh-termcolors
Obsoletes: shellfu-bash-core < 0.10
%description bash-pretty
This sub-package contains 'pretty', universal Shellfu logging module.

%package bash-sfdoc
Summary: Shellfu/Bash module to export docs from Shellfu modules
Requires: %{pod2manpkg}
Requires: perl
Requires: shellfu-bash
Requires: shellfu-bash-pretty
%description bash-sfdoc
This sub-package contains Shellfu/Bash module to export documentation
from other Shellfu modules that follow Shellfu coding style.

%package bash-sfpi
Summary: Shellfu/Bash plugin interface
Requires: shellfu-bash
Requires: shellfu-bash-pretty
%description bash-sfpi
This sub-package contains Shellfu/Bash module to help manage, discover,
select and load other Shellfu modules as plugins.

%package devel
Summary: Essential developer tools
Requires: %{manpkg}
Requires: shellfu-bash-pretty
Requires: shellfu-bash-sfdoc
%description devel
This sub-package contains tools useful mostly for development of shellfu
scripts.

%package sh
Summary: Generic base
Requires: shellfu
%description sh
This sub-package contains POSIX-oriented parts of library infrastructure.
Generic Shellfu modules aiming for POSIX compatibility should depend on this.

%package sh-coerce
Summary: Shellfu/sh module with data coercion helpers
Requires: perl
%if "%{legacy_coerce}" == "false"
Requires: perl-Term-ANSIColor
%endif
Requires: shellfu-sh
%description sh-coerce
This sub-package contains 'coerce', Shellfu module containing few
data coercion helpers.

%package sh-exit
Summary: Shellfu/sh module for standard exit statuses
Requires: shellfu-sh
Obsoletes: shellfu-bash-core < 0.10
%description sh-exit
This sub-package contains 'exit', Shellfu module containing functions
and variables for standard exit statuses.

%package sh-isa
Summary: Shellfu/sh module with validation helpers
Requires: shellfu-sh
%description sh-isa
This sub-package contains 'isa', Shellfu module containing few
value validation helpers.

%package sh-termcolors
Summary: Shellfu/sh module for terminal color names
Requires: shellfu-sh
Obsoletes: shellfu-bash-core < 0.10
%description sh-termcolors
This sub-package contains 'termcolors', Shellfu module containing most
common ANSI color names.

%prep
%setup -q
/bin/sed -i '/^__COERCE__LEGACY=/s/:-false/:-%{legacy_coerce}/' ./src/include-sh/coerce.sh


%build
make %{?_smp_mflags} PREFIX=/usr

%install
%make_install PREFIX=/usr

%check
make test \
    PATH=%{buildroot}/%{_bindir}:$PATH \
    SHELLFU_INCLUDE=%{buildroot}/%{_datadir}/%{name}/include \
    SHELLFU_DIR=%{buildroot}/%{_datadir}/%{name}

%files
%config %{_sysconfdir}/bash_completion.d/%{name}.bash
%dir %{_datadir}/%{name}
%dir %{_docdir}/%{name}
%doc %{_docdir}/%{name}/LICENSE.md
%doc %{_docdir}/%{name}/README.md
%{_bindir}/sfpath
%{_datadir}/%{name}/shellfu.sh

%files bash
%dir %{_datadir}/%{name}/include-bash

%files bash-arr
%{_datadir}/%{name}/include-bash/arr.sh

%files bash-pretty
%{_datadir}/%{name}/include-bash/_pretty_color.sh
%{_datadir}/%{name}/include-bash/_pretty_forcecolor.sh
%{_datadir}/%{name}/include-bash/_pretty_html.sh
%{_datadir}/%{name}/include-bash/_pretty_journald.sh
%{_datadir}/%{name}/include-bash/_pretty_notify.sh
%{_datadir}/%{name}/include-bash/_pretty_plain.sh
%{_datadir}/%{name}/include-bash/pretty.sh

%files bash-sfdoc
%{_datadir}/%{name}/include-bash/sfdoc.sh

%files bash-sfpi
%{_datadir}/%{name}/include-bash/sfpi.sh

%files devel
%config %{_sysconfdir}/bash_completion.d/%{name}-devel.bash
%{_bindir}/sfdoc
%{_bindir}/sfembed
%{_bindir}/sfembed_pod

%files sh
%dir %{_datadir}/%{name}/include-sh

%files sh-coerce
%{_datadir}/%{name}/include-sh/coerce.sh

%files sh-exit
%{_datadir}/%{name}/include-sh/exit.sh

%files sh-isa
%{_datadir}/%{name}/include-sh/isa.sh

%files sh-termcolors
%{_datadir}/%{name}/include-sh/termcolors.sh

%changelog

# specfile built with MKit __MKIT_MKIT_VERSION__
