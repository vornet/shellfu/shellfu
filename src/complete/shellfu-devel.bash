__sfdoc() {
    local cur       # current word
    local prev      # previous word
    local opts      # options
    local rest      # rest of options
    local cmds      # commands
    local crtext    # text for COMPREPLY
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts="-d -O -a -I -o --debug --object --all --include --only-from --encoding --name"
    cmds="-l -L -s -e --ls --lsvar --lsfun --which --lsmod --src --export"
    rest="${opts/$prev/}"
    crtext=$(
        case $prev in
            sfdoc)                  echo "$opts $cmds"; __sfdoc_compgen_M2 ;;
            -d|--debug)             echo "$rest $cmds"; __sfdoc_compgen_M3 ;;
            -O|--object)            echo "$rest $cmds"; __sfdoc_compgen_M3 ;;
            -a|--all)               echo "$rest $cmds"; __sfdoc_compgen_M3 ;;
            -I|--include)           compgen -o nospace -d -- "$cur" ;;
            -o|--only-from)         compgen -o nospace -d -- "$cur" ;;
            --encoding)             iconv -l | tr '[:upper:]' '[:lower:]' | sed s://$:: ;;
            --name)                 echo "" ;;
            -e|--export)            echo "markdown manpage pod" ;;
            -s|--src)               __sfdoc_compgen_M3 ;;
            -l|--ls)                __sfdoc_compgen_M2 ;;
            --lsfun|--lsvar)        __sfdoc_compgen_M2 ;;
            --which)                sfdoc --lsmod ;;
            markdown|manpage|pod)   __sfdoc_cwhas "-e" "--export" \
                                      && __sfdoc_compgen_M3  ;;
        esac
    )
    COMPREPLY=(
        $(compgen -W "$crtext" -- "$cur")
    )
}

__sfdoc_compgen_M2() {
    #
    # Produce list of possible MODULE values
    #
    case $cur in
        */*)    compgen -f -o nospace -- "$cur" ;;
        *)      sfdoc --lsmod ;;
    esac
}

__sfdoc_cwhas() {
    #
    # True if COMP_WORDS already has word $1 or $2...
    #
    local word
    local want
    for want in "$@"; do
        for word in "${COMP_WORDS[@]}"; do
            test "$word" == "$want" && return 0
        done
    done
    return 1
}

__sfdoc_compgen_M3() {
    #
    # Produce list of possible MODULE values
    #
    if __sfdoc_cwhas "-O" "--object"; then
        sfdoc --ls | cut -d: -f2
    else
        __sfdoc_compgen_M2
    fi
}

complete -F __sfdoc sfdoc
