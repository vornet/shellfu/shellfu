
__sfpath() {
    local cur       # current word
    local prev      # previous word
    local lopts     # long options
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    lopts="--version --version-semver"
    COMPREPLY=(
        $(compgen -W "$lopts" -- "$cur")
    )
}

complete -F __sfpath sfpath
