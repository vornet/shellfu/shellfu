#!/bin/bash

shellfu import termcolors


_PRETTY_FORCECOLOR_DEBUG="${TERMCOLORS_LBLUE}"
_PRETTY_FORCECOLOR_DIE="${TERMCOLORS_LRED}"
_PRETTY_FORCECOLOR_USAGE_IS="${TERMCOLORS_YELLOW}"
_PRETTY_FORCECOLOR_THINK="${TERMCOLORS_LBLACK}"
_PRETTY_FORCECOLOR_WARN="${TERMCOLORS_LRED}"
_PRETTY_FORCECOLOR_OFF="${TERMCOLORS_NONE}"


_pretty__debug() {
    local decor="()"
    local PrettyCallerIsMain=${PrettyCallerIsMain:-false}
    local PrettyCaller=${PrettyCaller:-UNKNOWN}
    $PrettyCallerIsMain && decor=
    while IFS= read -r line;
    do echo -ne "${_PRETTY_FORCECOLOR_DEBUG}debug:$PrettyCaller$decor:$_PRETTY_FORCECOLOR_OFF"
       echo "$line"; done
}


_pretty__die() {
    echo -ne "$_PRETTY_FORCECOLOR_DIE"
    while IFS= read -r line;
    do echo "$line"; done
    echo -ne "$_PRETTY_FORCECOLOR_OFF"
}


_pretty__mkhelp() {
    echo -ne "$_PRETTY_FORCECOLOR_USAGE_IS"
    while IFS= read -r line;
    do echo -e "$line"; done
    echo -ne "$_PRETTY_FORCECOLOR_OFF"
}


_pretty__mkusage() {
    echo -ne "$_PRETTY_FORCECOLOR_USAGE_IS"
    while IFS= read -r line;
    do echo -e "$line"; done
    echo -ne "$_PRETTY_FORCECOLOR_OFF"
}


_pretty__think() {
    echo -ne "$_PRETTY_FORCECOLOR_THINK"
    while IFS= read -r line;
    do echo "$line"; done
    echo -ne "$_PRETTY_FORCECOLOR_OFF"
}


_pretty__warn() {
    echo -ne "$_PRETTY_FORCECOLOR_WARN"
    while IFS= read -r line;
    do echo "$line"; done
    echo -ne "$_PRETTY_FORCECOLOR_OFF"
}
