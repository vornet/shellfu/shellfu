#!/bin/bash

shellfu import termcolors


_PRETTY_COLOR_DEBUG=""
_PRETTY_COLOR_DIE=""
_PRETTY_COLOR_USAGE_IS=""
_PRETTY_COLOR_THINK=""
_PRETTY_COLOR_WARN=""
_PRETTY_COLOR_OFF=""


__shellfu__pretty_color__init() {
    test -t 2 || return 0   # stderr is a pipe/file; refrain from colors
    _PRETTY_COLOR_DEBUG="${TERMCOLORS_LBLUE}"
    _PRETTY_COLOR_DIE="${TERMCOLORS_LRED}"
    _PRETTY_COLOR_USAGE_IS="${TERMCOLORS_YELLOW}"
    _PRETTY_COLOR_THINK="${TERMCOLORS_LBLACK}"
    _PRETTY_COLOR_WARN="${TERMCOLORS_LRED}"
    _PRETTY_COLOR_OFF="${TERMCOLORS_NONE}"
}


_pretty__debug() {
    local decor="()"
    local PrettyCallerIsMain=${PrettyCallerIsMain:-false}
    local PrettyCaller=${PrettyCaller:-UNKNOWN}
    $PrettyCallerIsMain && decor=
    while IFS= read -r line;
    do echo -ne "${_PRETTY_COLOR_DEBUG}debug:$PrettyCaller$decor:$_PRETTY_COLOR_OFF"
       echo "$line"; done
}


_pretty__die() {
    echo -ne "$_PRETTY_COLOR_DIE"
    while IFS= read -r line;
    do echo "$line"; done
    echo -ne "$_PRETTY_COLOR_OFF"
}


_pretty__mkhelp() {
    echo -ne "$_PRETTY_COLOR_USAGE_IS"
    while IFS= read -r line;
    do echo -e "$line"; done
    echo -ne "$_PRETTY_COLOR_OFF"
}


_pretty__mkusage() {
    echo -ne "$_PRETTY_COLOR_USAGE_IS"
    while IFS= read -r line;
    do echo -e "$line"; done
    echo -ne "$_PRETTY_COLOR_OFF"
}


_pretty__think() {
    echo -ne "$_PRETTY_COLOR_THINK"
    while IFS= read -r line;
    do echo "$line"; done
    echo -ne "$_PRETTY_COLOR_OFF"
}


_pretty__warn() {
    echo -ne "$_PRETTY_COLOR_WARN"
    while IFS= read -r line;
    do echo "$line"; done
    echo -ne "$_PRETTY_COLOR_OFF"
}
