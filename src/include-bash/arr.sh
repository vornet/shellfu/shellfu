#!/bin/bash

shellfu import isa
shellfu import pretty

#
# Array utilities
#
# Several utilities for loading and printing Bash arrays.
#

arr__after() {
    #
    # Return value after value $1 is in array $2
    #
    # Usage:
    #     arr__after VALUE ARRAY
    #
    # Example:
    #
    #     names=( Alice Bob Claire "" Eva )
    #     arr__after Bob names
    #     # prints 'Claire' and returns with 0
    #     arr__after Claire names
    #     # prints '' and returns with 0
    #     arr__after Eva names
    #     # prints nothing and returns with 1
    #     arr__after Joe names
    #     # prints nothing and returns with 2
    #
    local want=$1
    local iarr=$2
    local copy
    local value
    local code
    local aim_next=false
    __arr__val_iarr "$iarr" || return 2
    code=$(declare -p "$iarr"); code=${code/ $iarr=/ copy=}
    eval "$code"
    test ${#copy[*]} -gt 0 || return 1
    for value in "${copy[@]}"; do
        $aim_next && echo -n "$value" && return 0   # aimed in prev iteration
        test "$value" == "$want" && aim_next=true   # next must be the desired one
    done
    $aim_next && return 1   # there was no next item
    return 2
}

arr__fromcmd() {
    #
    # Save output lines from command $2.. to array named $1
    #
    # Usage:
    #     arr__fromcmd ARRAY CMD [ARG]..
    #
    # Unlike `mapfile -t <<<"$(command)", this produces zero
    # items if command output was empty.
    #
    local __arr__fromcmd__oarr=$1; shift
    local __arr__fromcmd__cmd=("$@")
    local __arr__fromcmd__tmp
    local __arr__fromcmd__es
    __arr__val_oarr "$__arr__fromcmd__oarr" || return 2
    __arr__fromcmd__tmp=$(mktemp /tmp/arr__fromcmd.__arr__fromcmd__tmp.XXXXX)
    "${__arr__fromcmd__cmd[@]}" > "$__arr__fromcmd__tmp"; __arr__fromcmd__es=$?
    mapfile -t "$__arr__fromcmd__oarr" <"$__arr__fromcmd__tmp"
    rm "$__arr__fromcmd__tmp"
    return $__arr__fromcmd__es
}

arr__has() {
    #
    # True if value $1 is in array $2
    #
    # Usage:
    #     arr__has VALUE ARRAY
    #
    local want=$1
    local iarr=$2
    local copy
    local value
    local code
    __arr__val_iarr "$iarr" || return 2
    code=$(declare -p "$iarr"); code=${code/ $iarr=/ copy=}
    eval "$code"
    for value in "${copy[@]}"; do
        test "$value" == "$want" && return 0
    done
    return 1
}

arr__join() {
    #
    # Join items in array $2 by string $1
    #
    # Usage:
    #     arr__join DELIM ARRAY
    #
    # Example:
    #
    #     names=( Alice Bob )
    #     arr__join , names
    #     # prints 'Alice,Bob'
    #
    local delim=$1
    local iarr=$2
    local first=true
    local item
    local copy
    local code
    __arr__val_iarr "$iarr" || return 2
    code=$(declare -p "$iarr"); code=${code/ $iarr=/ copy=}
    eval "$code"
    for item in "${copy[@]}"; do
        $first || echo -n "$delim"
        first=false
        echo -n "$item"
    done
}

__arr__val_iarr() {
    #
    # Validate input array named $1
    #
    local name=$1
    set | grep -q "^$name=" || { warn "no such array: $name"; return 2; }
}

__arr__val_oarr() {
    #
    # Validate output array named $1
    #
    local name=$1
    isa__name "$name" || { warn "invalid array name: $name"; return 2; }
}
