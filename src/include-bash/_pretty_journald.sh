#!/bin/bash


_pretty__debug() {
    local decor="()"
    local PrettyCallerIsMain=${PrettyCallerIsMain:-false}
    local PrettyCaller=${PrettyCaller:-UNKNOWN}
    $PrettyCallerIsMain && decor=
    sed "s/^/debug:$PrettyCaller$decor: /" | systemd-cat -p debug -t "${0##*/}"
}

_pretty__die() {
    systemd-cat -p crit -t "${0##*/}"
}

_pretty__mkhelp() {
    systemd-cat -p notice -t "${0##*/}"
}

_pretty__mkusage() {
    systemd-cat -p notice -t "${0##*/}"
}

_pretty__think() {
    systemd-cat -p info -t "${0##*/}"
}

_pretty__warn() {
    systemd-cat -p warning -t "${0##*/}"
}
