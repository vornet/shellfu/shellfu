#!/bin/bash


_pretty__debug() {
    local class=caller_fn
    local PrettyCallerIsMain=${PrettyCallerIsMain:-false}
    local PrettyCaller=${PrettyCaller:-UNKNOWN}
    $PrettyCallerIsMain && class=caller_bin
    echo -n "<pre class='debug'><span class='$class'>$PrettyCaller</span>"
    cat
    echo "</pre>"
}


_pretty__die() {
    echo -n "<pre class='fatal'>"
    cat
    echo "</pre>"
}


_pretty__mkhelp() {
    echo -n "<pre class='help'>"
    cat
    echo "</pre>"
}


_pretty__mkusage() {
    echo -n "<pre class='usage'>"
    cat
    echo "</pre>"
}


_pretty__think() {
    echo -n "<pre class='think'>"
    cat
    echo "</pre>"
}


_pretty__warn() {
    echo -n "<pre class='warning'>"
    cat
    echo "</pre>"
}
