#!/bin/bash


_pretty__debug() {
    local decor="()"
    local PrettyCallerIsMain=${PrettyCallerIsMain:-false}
    local PrettyCaller=${PrettyCaller:-UNKNOWN}
    $PrettyCallerIsMain && decor=
    while IFS= read -r line;
    do echo "debug:$PrettyCaller$decor: $line"; done
}

_pretty__die() {
    while IFS= read -r line;
    do echo "$line"; done
}

_pretty__mkhelp() {
    while IFS= read -r line;
    do echo "$line"; done
}

_pretty__mkusage() {
    while IFS= read -r line;
    do echo "$line"; done
}

_pretty__think() {
    while IFS= read -r line;
    do echo "$line"; done
}

_pretty__warn() {
    while IFS= read -r line;
    do echo "$line"; done
}
