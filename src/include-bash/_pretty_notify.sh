#!/bin/bash


_pretty__debug() {
    local decor="()"
    local PrettyCallerIsMain=${PrettyCallerIsMain:-false}
    local PrettyCaller=${PrettyCaller:-UNKNOWN}
    $PrettyCallerIsMain && decor=
    notify-send -u low -a "${0##*/}" debug "debug:$PrettyCaller$decor: $(cat)"
}

_pretty__die() {
    notify-send -u critical -a "${0##*/}" fatal "$(cat)"
}

_pretty__mkhelp() {
    notify-send -a "${0##*/}" help "$(cat)"
}

_pretty__mkusage() {
    notify-send -u critical -a "${0##*/}" usage "$(cat)"
}

_pretty__think() {
    notify-send -a "${0##*/}" info "$(cat)"
}

_pretty__warn() {
    notify-send -u critical -a "${0##*/}" warning "$(cat)"
}
