#!/bin/sh

#
# Validation helpers
#
# This module provides several basic validation functions
# to help make code more readable and avoid having to implement
# them (some may be more tricky than they look like).
#

isa__bool() {
    #
    # True if $1 is a boolean
    #
    case $1 in
        true)   return 0 ;;
        false)  return 0 ;;
        *)      return 1 ;;
    esac
}

isa__false() {
    #
    # True if $1 is a False boolean
    #
    test "$1" == false
}

isa__int() {
    #
    # True if $1 is an integer
    #
    # Note that $1 does not have to be decimal; in POSIX shells,
    # eg. 0xf1 is a valid hexadecimal integer and 0755 is a valid
    # octal integer.
    #
    {
        test -z "$1"    && return 1
        test "$1" -ge 0 && return 0
        return 1
    } 2>/dev/null
}

isa__num() {
    #
    # True if $1 is a simple dot-separated float
    #
    # "simple dot-separated float" means a number in this form:
    #
    #     [DIGITS].DIGITS
    #     -[DIGITS].DIGITS
    #
    # where DIGITS is a contiguous string of digits and
    # diguts preceding the period are optional.
    #
    {
        echo "$1" | grep -Eqx '[-]?[[:digit:]]*[.][[:digit:]]+' && return 0
        echo "$1" | grep -Eqx '[-]?[[:digit:]]+' && return 0
        return 1
    } 2>/dev/null
}

isa__name() {
    #
    # True if $1 is a name in most languages
    #
    echo "$1" | grep -qx '[[:alpha:]_][[:alnum:]_]*'
}

isa__posint() {
    #
    # True if $1 is a positive integer
    #
    {
        test -z "$1"    && return 1
        test "$1" -ge 0 && return 0
        return 1
    } 2>/dev/null
}

isa__posnum() {
    #
    # True if $1 is a positive simple dot-separated float
    #
    # Same as isa__num(), but minus sign is prohibited:
    #
    #     [DIGITS].DIGITS
    #
    # where DIGITS is a contiguous string of digits and
    # diguts preceding the period are optional.
    #
    {
        echo "$1" | grep -Eqx '[[:digit:]]*[.][[:digit:]]+' && return 0
        echo "$1" | grep -Eqx '[[:digit:]]+' && return 0
        return 1
    } 2>/dev/null
}

isa__true() {
    #
    # True if $1 is a True boolean
    #
    test "$1" == true
}
