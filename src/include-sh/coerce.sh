#!/bin/sh

#
# Data coercion helpers
#
# This module provides several simple functions to help transform
# data to fit certain constraints.
#

#
# Replacement character
#
COERCE__REPCHAR=${COERCE__REPCHAR:-�}

coerce__nocolor() {
    #
    # Remove ANSI color codes
    #
    if $__COERCE__LEGACY; then
        sed 's/\x1b\[[0-9;]*m//g'
    else
        perl -CS -Mutf8 -MTerm::ANSIColor=colorstrip -ne 'print colorstrip $_;'
    fi
}

coerce__noctl() {
    #
    # Replace non-printable characters with $COERCE__REPCHAR
    #
    # Keep only characters that have grapgical representation ([:graph:] POSIX
    # class), newline, tab and space.  Replace rest with $COERCE__REPCHAR.
    #
    perl -CS -Mutf8 -pe "s|[^[:graph:] \t\n]|$COERCE__REPCHAR|g"
}

coerce__nofdraw() {
    #
    # Replace frame-drawing characters with ASCII
    #
    # Replace frame-drawing characters according
    # to following mapping:
    #
    #     ┌ ┬ ┐ └ ┴ ┘ ├ ┤ │ ┼ ─
    #
    #     ' ' ' . . . | | | | -
    #
    # This converts frame-drawing to ASCII, making it
    # safer when fonts on terminals are not rendering
    # properly.
    #
    perl -CS -Mutf8 -pe "
        tr{┌┬┐}{.};
        tr{└┴┘}{'};
        tr{├┼┤│}{|};
        tr{─}{-};
    "
}

coerce__for_yaml() {
    #
    # Replace yaml-invalid characters
    #
    # Yaml won't allow all characters:
    #
    # > [...] The allowed character range explicitly excludes the C0 control
    # > block #x0-#x1F (except for TAB #x9, LF #xA, and CR #xD which are
    # > allowed), DEL #x7F, the C1 control block #x80-#x9F (except for NEL
    # > #x85 which is allowed), the surrogate block #xD800-#xDFFF, #xFFFE,
    # > and #xFFFF.
    #
    # so take stdin and replace all unacceptable characters with '�'.
    #
    perl -CS -Mutf8 -pe "tr/$(__coerce__for_yaml_bad)/$COERCE__REPCHAR/"
}

#
# Legacy mode
#
# If 'true', avoids using Term::ANSIColor in favor of a local hacky
# sed expression yanked from stackoverflow.  Use if Term::ANSIColor
# is not available (as is case of eg. RHEL-6).
#
__COERCE__LEGACY=${__COERCE__LEGACY:-false}

__coerce__for_yaml_bad() {
    #
    # Print all YAML-bad chars
    #
    printf '\N{U+0}-\N{U+8}\N{U+B}\N{U+C}\N{U+E}-\N{U+1F}'    # C0 with some gaps
    printf '\N{U+7F}'                                         # DEL alone
    printf '\N{U+80}-\N{U+84}\N{U+86}-\N{U+9F}'               # C1 with NEL gap
    # printf -n '\N{U+D800}-\N{U+DFFF}'                        # surrogates
    # printf -n '\N{U+FFFE}-\N{U+FFFF}'                        # 0xFFFE and 0xFFFF
    #FIXME: for some reasons perl complains about these ^^
}
