#!/bin/sh


EXIT_OK=0
EXIT_NO=1
EXIT_USAGE=2
EXIT_ERROR=3
EXIT_PANIC=4


exit_ok() {
    #
    # Exit script with success
    #
    exit $EXIT_OK
}


exit_no() {
    #
    # Exit script with answer "no"
    #
    exit $EXIT_NO
}


exit_usage() {
    #
    # Exit script with usage error
    #
    exit $EXIT_USAGE
}


exit_error() {
    #
    # Exit script with generic unexpected error
    #
    exit $EXIT_ERROR
}


exit_panic() {
    #
    # Exit script in panic (e.g. assert failure, sure bug)
    #
    exit $EXIT_PANIC
}
