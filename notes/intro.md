The Why of Shellfu
------------------

Most shell languages don't have libraries.

You might think: "Are you kidding me?  Libraries are for real serious
programmers, and real serious programmers don't real-serious-program
in shell!  So how is that a problem lol."

I mostly agree.  Shell languages, by definition are command languages,
basically meant to allow you kick around files, create directories and
most importantly, launch *other* real serious programs.

What shell languages can also be used for is gluing together some of
these real serious programs so that you don't have to type everything
over and over.  These glue scripts are normally nothing serious, just
copy this there and launch that with that piped in.  Done.

*Granted,* in some cases, you simply don't stop there.  Sometimes you
create one script here, one script there, one over there.  When you
maintain a system, sometimes you do write scripties here and there.
That's OK, all of them are just few lines; trivial things with obvious
purpose, right?

But sometimes, you don't work alone.  Sometimes there's bunch of other
folks and you want to work together so that when you take vacation and
something breaks, your colleagues can use your scripts instead of
needing to do everything manually or call you.

So even if it's not real serious web application or real serious kernel,
*some* level of re-usability pays off.


### OK so what ###

It's not that it's not possible to build a library from a bunch of scripts
and import all with the magical dot command, but let's be honest: you end
up either with swarm of modules of varying style, maturity and quality
*and* overlapping scopes, or one humongous beast-of-a-module with pack
of mysterious functions and variables that everyone is scared to touch.

Unless you take it seriously and start thinking *way* ahead from the
start.

Which is exactly what I did.

Finding myself in swarm of varying scripties, constantly trying to keep
them at least a bit tidy, I decided to stop and do it another way:  Just
try and see how much of the magic, the modularity, maintainability and
discoverability that's so common in other platforms could be taken into
the dark, blunt world of shell languages.

Result of these efforts is called Shellfu.


### So what does it do? ###

Shellfu, as a project consists of three parts:

 *  `shellfu()`, the shell function that lets you import a module from
    known location just by name, therefore decoupling the *deployment*
    from *the dependency*.

 *  Shellfu core library, providing few modules readily usable for
    some of the most common tasks (printing messages for user, loading
    basic INI files...).

    Note that this "library" is currently extremely tiny with about
    4-6 modules.  It's currently (July 2016) not clear what modules
    should be added or removed and what should be the size, although
    there are no plans to add a lot.  Most of advantage of shellfu
    is to be gained from writing *domain-specific* modules.

 *  Shellfu Style Guide, setting common standard for shell modules,
    allowing you to collaborate on the code in an efficient manner
    (in-line documentation, peer review, code readability...).

 *  As a bonus, *sfdoc*, building upon the Shellfu Style Guide,
    allows your user to discover and read documentation of installed
    modules (akin to *perldoc* or *pydoc*).


### Schizophrenia kicks in ###

> *So are we talking POSIX shell, Bash or Zsh or what?*

The basic principle is, albeit untested, meant to be extensible to most
common *bourne-like* shell languages.  In more specific sense, this
means two things:

 *  *shellfu.sh*, the core part of the code should be compatible with
    *dash*, the Debian Almquist Shell,

 *  although I don't expect various-language modules to co-exist,
    *shellfu.sh* aims to be able to filter modules based on interpreter
    (more on that later).

 *  and some modules of the core library will try to be POSIX/dash
    compatible, which will make them loadable from all.

Note that I'm kind of using terms "POSIX" and "dash" interchangeably.
What I really mean is that since POSIX is rather vague in many places,
and there are kajilions of POSIX shells, it's impractical to aim for
*real* POSIX compatibility.  Therefore, the "POSIX-oriented" code will
be tested under *dash*, plus set of most common interpreters: `bash`,
(add here).

I reckon that's about as POSIXey as one can get without going mad.
